use clap::Parser;

mod ast;
mod lexer;
mod parser;

fn eval(s: &str) -> Result<f64, String> {
  let l = lexer::Lexer::new(s);
  let mut p = parser::Parser::new(l);
  let tree = p.parse()?;
  ast::eval(tree)
}

#[derive(clap::Parser)]
struct CliOpt {
  #[clap(short, long)]
  expr: Option<String>,
}

fn main() {
  let opt = CliOpt::parse();
  match opt.expr {
    Some(expr) => match eval(&expr) {
      // print result and exit:
      Ok(x) => println!("{}", x),
      Err(msg) => eprintln!("error: {}", msg),
    },
    None => {
      // fire up REPL:
      let mut rl = rustyline::DefaultEditor::new().expect("could not create line-reader");
      loop {
        let readline = rl.readline(">> ");
        match readline {
          Ok(line) => match eval(line.as_str()) {
            Ok(x) => println!("result={}", x),
            Err(msg) => eprintln!("{}", msg),
          },
          Err(rustyline::error::ReadlineError::Interrupted) => {
            break;
          }
          Err(rustyline::error::ReadlineError::Eof) => {
            break;
          }
          Err(err) => {
            println!("error: {:?}", err);
            break;
          }
        }
      }
    }
  }
}
